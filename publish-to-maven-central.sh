#!/bin/bash

# Usage:
# set environment variables: SONATYPE_USER, SONATYPE_PASSWORD
# ./publish-to-maven-central.sh VERSION [MODULE]
# eg: ./publish-to-maven-central.sh 1.0.0

modules=(
commons-micronaut
commons-micronaut-pg
commons-sequence
commons-tenant
commons-util-spring
)

ver=$1
module=$2
[[ -z "$ver" ]] && echo "give me a version" && exit 1
gr=./gradlew
out=publish
user=$SONATYPE_USER
pw=$SONATYPE_PASSWORD
auth=$(echo "${user}:${pw}" | base64)

build() {
  ./gradlew -Pversion=$ver clean assemble generatePom sign || exit 1
}

publish() {
  p=$1
  mkdir $out/$p

  cp -r $p/build/libs/* $out/$p
  cp -r $p/build/publications/mavenJava/pom-default.xml $out/$p/$p-$ver.pom
  cp -r $p/build/publications/mavenJava/pom-default.xml.asc $out/$p/$p-$ver.pom.asc

  cd $out/$p
  for file in ./*; do md5sum "$file" | awk '{print $1}' > "$file".md5; done
  for file in ./*; do sha1sum  "$file" | awk '{print $1}' > "$file".sha1; done
  rm -rf *.md5.sha1

  mkdir -p com/kodality/commons/$p/$ver
  mv * com/kodality/commons/$p/$ver
  zip=$p-$ver.zip
  zip -r $zip com

  curl -XPOST -H "Authorization: Bearer $auth" --form bundle=@$zip -D - "https://central.sonatype.com/api/v1/publisher/upload?publishingType=AUTOMATIC"
  cd -
}


rm -rf $out && mkdir -p $out || exit 1

build

if [ ! -z "$module" ]; then
  publish $module
  exit 0
fi

for p in ${modules[@]}; do
  publish $p
done



