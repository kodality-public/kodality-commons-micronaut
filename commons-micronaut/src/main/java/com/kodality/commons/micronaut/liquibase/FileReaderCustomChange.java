package com.kodality.commons.micronaut.liquibase;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public abstract class FileReaderCustomChange implements CustomTaskChange {
  protected List<String> files;
  protected ResourceAccessor resourceAccessor;

  protected abstract void handleFile(String name, byte[] content);

  public void setFiles(String files) {
    this.files = Arrays.asList(StringUtils.split(files));
  }

  @Override
  public void setFileOpener(ResourceAccessor resourceAccessor) {
    this.resourceAccessor = resourceAccessor;
  }

  @Override
  public String getConfirmationMessage() {
    return null;
  }

  @Override
  public void setUp() throws SetupException {
  }

  @Override
  public ValidationErrors validate(Database database) {
    return null;
  }

  @Override
  public void execute(Database database) throws CustomChangeException {
    if (files == null) {
      return;
    }
    files.forEach(file -> {
      try {
        InputStream is = resourceAccessor.get(file).openInputStream();
        byte[] content = IOUtils.toByteArray(is);
        String name = StringUtils.substringAfterLast(file, "/");
        handleFile(name, content);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    });

  }

  protected String asString(byte[] content) {
    if (content == null) {
      return null;
    }
    try {
      return new String(content, "UTF8");
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }
}
