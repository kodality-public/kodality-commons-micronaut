package com.kodality.commons.acl;

public interface AclAccess {
  String owner = "owner";
  String consume = "consume";
  String edit = "edit";
  String view = "view";
}
